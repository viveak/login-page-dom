const uname = document.getElementById("username");
const email = document.getElementById("email");
const password = document.getElementById("password");


if (document.title == "SignUp") {
    document
        .getElementById("register")
        .addEventListener("click", function myFunction() {

            if (isEmail(email.value)) {
                email.style.borderBottom = "2px solid #9eaaff";
            } else {
                email.style.borderBottom = "2px solid Red";
            }

            if (uname.value.length > 2) {
                uname.style.borderBottom = "2px solid #9eaaff";

            } else {
                uname.style.borderBottom = "2px solid Red";
            }

            if (isPassword(password.value)) {
                password.style.borderBottom = "2px solid #9eaaff";
            } else {
                password.style.borderBottom = "2px solid Red";
            }
        });
} else if (document.title == "LogIn") {


    document
        .getElementById("login")
        .addEventListener("click", function myFunction() {

            if (uname.value.length > 2) {
                uname.style.borderBottom = "2px solid #9eaaff";

            } else {
                uname.style.borderBottom = "2px solid Red";
            }

            if (isPassword(password.value)) {
                password.style.borderBottom = "2px solid #9eaaff";
            } else {
                password.style.borderBottom = "2px solid Red";
            }
        });

}

function isEmail(email) {
    return /^([a-z\d\.-]+)@([a-z]+)(\.)([a-z]{2,10})$/.test(email);
}

function isPassword(password) {

    if (password.length > 8) {
        return true;
    } else {
        return false;
    }
}